
# Scrivener Word Count to Google Sheets

  

I'm an avid Scriviner user, one thing I find the app doesn't do well is when it comes to word count analytics. You could just note down your daily word count in a excel sheet or a tracker but I often forget when other things come up. So I did some diving into the project folders and learnt that the .scrivx file you use for the project is just an XML file that can be parsed.

Using the Scrivx file for your project along with the recents.txt file in the Project\Settings folder it can take the Previous session word count along with the last opened text document in your project and import them into a Google Sheet. This can be manually run or run on a schedule through something like a Cron job or Windows task scheduler. The template Google sheet can be found [here](https://docs.google.com/spreadsheets/d/1Ce_PQV9tKc9s9PAB-LDiEtRBYhg_ifLn0wIw1NURZHc/edit?usp=sharing), just make a copy of it.

  ![enter image description here](https://i.imgur.com/VQEdnZU.png)
  

## Things to Note

  

- They way this script works is all determined by your session count in Scrivener, I'd suggest setting the job up as a scheduled task to run daily and set your Scrivener to clear the previous session at a set time each day just after the script runs.

- The google sheet can be altered, the way it is setup is to record the Date, Article name, at what act in the story it is and the word count. The second sheet notes the corresponding document name and act. The total word count tracker can be altered and currently will auto update depending on the counts noted by the script.

  

## Prerequisites

### Gspread

This script uses the Gspread python API to append the Google Sheets, this can be downloaded from https://github.com/burnash/gspread

  

### Google Developer Console

You will need to generate a Google API account, the [following article](https://www.analyticsvidhya.com/blog/2020/07/read-and-update-google-spreadsheets-with-python/) will walk you through obtaining the JSON file used to connect.
