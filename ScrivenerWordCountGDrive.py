import os
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import lxml
# define the scope
scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']

cwd = os.getcwd()
print(cwd)
# add credentials to the account
creds = ServiceAccountCredentials.from_json_keyfile_name(r'GOOGLE API ACCOUNT JSON', scope)

# authorize the clientsheet 
client = gspread.authorize(creds)

#get the instance of the Spreadsheet
sheet = client.open('DOCUMENT NAME IN GDRIVE')

# get the first sheet of the Spreadsheet
sheet_instance = sheet.get_worksheet(0)

def next_available_row(sheet_instance):
    str_list = list(filter(None, sheet_instance.col_values(1)))
    return str(len(str_list)+1)

xml = 'PATH TO YOUR .scrivx'

import xml.etree.ElementTree as ET
tree = ET.parse(xml)
root = tree.getroot()

#PREVIOUS SESSION WORD AND DATE
for session in root.findall('ProjectTargets/PreviousSession'):
    Words = session.get('Words')
    FullDate = session.get('Date')
    DString = str(FullDate)
    ShortDate = DString[0:10]

    print(Words)
    print(ShortDate)
#We read the recent file for the UUID of the last opened page
recent = open(r'PATH TO THE SCRIVENER PROJECT FOLDER: \Settings\recents.txt')
first_line = recent.readline().strip()


#PREVIOUS SESSION XMLS ARE IFFY SO IT JOINS THE BINDER NAME FROM THE RECENTS FOLDER
UUID1 = './/BinderItem[@UUID='
UUID = '"'+first_line+'"'
UUID3 = ']/Title'

#acts
for Act in root.findall(UUID1+UUID+UUID3):
    AV = Act.text
    print(AV)

#you can update the below acells to fit your spreadsheet.
next_row = next_available_row(sheet_instance)
sheet_instance.update_acell("A{}".format(next_row), ShortDate)
sheet_instance.update_acell("B{}".format(next_row), AV)
sheet_instance.update_acell("D{}".format(next_row), Words)

exit
